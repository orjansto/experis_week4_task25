using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task25.Models
{
    public class Supervisor
    {
        private int? id = new Random(DateTime.Now.Second).Next(100,999);
        private string name = "unknown";
        private bool? isAvailable =false;
        private string level = "beginner";

        [Required(ErrorMessage = "Please enter an Id (number)")]
        public int? Id { get => id; set => id = value; }
        [Required(ErrorMessage="Please enter name")]
        public string Name { get => name; set => name = value; }
        [Required(ErrorMessage = "Please indicate availability")]
        public bool? IsAvailable { get => isAvailable; set => isAvailable = value; }
        [Required(ErrorMessage = "Please enter a competence level")]
        public string Level { get => level; set => level = value; }
    }
}
