﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task25.Models
{
    public static class SupervisorGroup
    {
        private static List<Supervisor> CurrentSupervisors = new List<Supervisor>();
        private static Random rand = new Random(DateTime.Now.Second);

        public static List<Supervisor> Supervisors
        {
            get { return CurrentSupervisors; }
        }
        public static void AddSupervisor(Supervisor newSupervisor)
        {
            CurrentSupervisors.Add(newSupervisor);
        }
        /// <summary>
        /// Populates CurrentSupervisors with n new Supervisors
        /// </summary>
        /// <param name="n">number(int) of new supervisors</param>
        public static void PopWithRandSupervisors(int n)
        {
            for (int i = 0; i < n; i++)
            {
                Supervisor sup = new Supervisor();
                sup.Name = RandomNameGen();
                sup.Id = rand.Next(100, 999);
                if (rand.Next(0, 6) > 3)
                {
                    sup.IsAvailable = true;
                }
                AddSupervisor(sup);
            }
        }
        private static string RandomNameGen()
        {
            int len = rand.Next(5, 12);
            string[] consonants = { "B", "C", "Ch", "D", "Dr", "F", "Fr", "G", "H", "J", "K", "L", "M", "L", "N", "P", "Q", "R", "S", "Sh", "T", "V", "W", "X" };
            string[] vowels = { "a", "e", "ei","i", "o", "u", "ae", "y" };
            string Name = "";
            Name += consonants[rand.Next(consonants.Length)];
            Name += vowels[rand.Next(vowels.Length)];
            int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
            while (b < len)
            {
                Name += consonants[rand.Next(consonants.Length)].ToLower();
                b++;
                Name += vowels[rand.Next(vowels.Length)];
                b++;
            }
            return Name;
        }
    }
}
