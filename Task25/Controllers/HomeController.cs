﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task25.Models;

namespace Task25.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            ViewBag.WelcomeMessage = "Testing!";
            ViewBag.Time = DateTime.Now.ToString();
            return View("MyFirstVIew");
        }


        [HttpGet]
        public IActionResult SupervisorInfo()
        {   
            return View();
        }


        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor)
        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.AddSupervisor(supervisor);
                return View("AddSupervisorConfirmation", supervisor);
            }
            else
            {
                return View();
            }
            
        }

        public IActionResult AllSupervisors(Supervisor supervisor)
        {
            return View(SupervisorGroup.Supervisors);
        }
        public IActionResult SupervisorsWithS()
        {
            return View(SupervisorGroup.Supervisors.Where(s => s.Name.Substring(0,1).ToLower().Equals("s")));
        }
        public IActionResult AddRandomSupervisors()
        {
            SupervisorGroup.PopWithRandSupervisors(4);
            return View("AllSupervisors",SupervisorGroup.Supervisors);
        }
    }
}
